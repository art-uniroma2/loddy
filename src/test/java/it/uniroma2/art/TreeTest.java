package it.uniroma2.art;

import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.loddy.structure.Node;
import it.uniroma2.art.loddy.structure.TupleTree;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;

public class TreeTest {

	public static void main(String[] args) throws Exception {
		
		OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(new ARTModelFactorySesame2Impl());
		TripleQueryModelHTTPConnection conn = fact.loadTripleQueryHTTPConnection("http://202.45.139.84:10035/catalogs/fao/repositories/agrovoc#");
		String query = "SELECT ?type ?uri ?lexical ?lang WHERE { "
			+ "<http://aims.fao.org/aos/agrovoc/c_330834> ?type ?uri. "
			+ "?uri a <http://www.w3.org/2008/05/skos-xl#Label>. "
			+ "?uri <http://www.w3.org/2008/05/skos-xl#literalForm> ?literalForm. "
			+ "bind (lang(?literalForm) as ?lang) "
			+ "bind (str(?literalForm) as ?lexical)}";
		TupleQuery tq = conn.createTupleQuery(QueryLanguage.SPARQL, query, "");
		TupleBindingsIterator result = tq.evaluate(false);
		conn.disconnect();
		
		TupleTree tree = groupTuples(result);
		Node root = tree.getRootNode();
		List<Node> children = root.getChildren();
		for (Node c : children){
			System.out.println("binding:\t" + c.getBinding() + "\nvalue:\t\t" + c.getValue());
			List<Node> subChildren = c.getChildren();
			for (Node sc : subChildren) {
				System.out.println("binding:\t" + sc.getBinding() + "\nvalue:\t\t" + sc.getValue());
				List<TupleBindings> tuples = sc.getTuples();
				for (TupleBindings t : tuples){
					String type = t.getBinding("type").getBoundValue().getNominalValue();
					String uri = t.getBinding("uri").getBoundValue().getNominalValue();
					String lexical = t.getBinding("lexical").getBoundValue().getNominalValue();
					String lang = t.getBinding("lang").getBoundValue().getNominalValue();
					System.out.println("type="+type+"\nuri="+uri+"\nlexical="+lexical+"\nlang="+lang);
				}
			}
			System.out.println();
		}
	}
	
	private static TupleTree groupTuples(TupleBindingsIterator tuples){
		List<String> bindings = new ArrayList<String>();
		bindings.add("lang");
		bindings.add("type");
		TupleTree tree = new TupleTree(bindings);
		while (tuples.hasNext()){
			System.out.println("---------------TUPLA-------------------");
			TupleBindings tuple = tuples.next();
			String type = tuple.getBinding("type").getBoundValue().getNominalValue();
			String lang = tuple.getBinding("lang").getBoundValue().getNominalValue();
			String uri = tuple.getBinding("uri").getBoundValue().getNominalValue();
			String lexical = tuple.getBinding("lexical").getBoundValue().getNominalValue();
			System.out.println("type="+type+"\nuri="+uri+"\nlexical="+lexical+"\nlang="+lang);
			
			tree.addTuple(tuple);
		}
		return tree;
	}

}
