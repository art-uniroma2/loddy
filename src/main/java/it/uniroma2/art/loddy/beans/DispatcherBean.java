package it.uniroma2.art.loddy.beans;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;

import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DispatcherBean {
	
	protected static Logger logger = LoggerFactory.getLogger(DispatcherBean.class);
	
	private ConnectionBean connectionBean;
	private Map<String, String> pageMapping;
	
	private String resType;

	/**
	 * connectionBean property injection
	 * @param connectionBean
	 */
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}
	
	/**
	 * pageMapping property injection
	 * @param pageMapping
	 */
	public void setPageMapping(Map<String, String> pageMapping) {
		this.pageMapping = pageMapping;
	}
	
	/**
	 * Checks if the resource requested exists. If not it redirect to not_found.xhtml.
	 * Otherwise, based on the accept header of the request, it responds with a 303 "see other". 
	 * @throws Exception 
	 */
	public void contentNegotiation() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		String localBaseUri = connectionBean.getLocalBaseURI();
		String resID = request.getParameter("resID");
		logger.debug("Content negotiation for resource with id: "+resID);
		
		resourceTypeDetect();
		if (resType.equals("unknown")){//resource doesn't exist
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			context.getApplication().getNavigationHandler().handleNavigation(context, null, "/not_found.xhtml");
			return;
		}
		//TODO decidere logica per priorit�
		String location;
		String accept = request.getHeader("accept");
		if (accept != null){
			if (accept.contains(RDFFormat.RDFXML.getMIMEType())){
				location = localBaseUri+resID+".rdf";
				response.addHeader("Content-Type", RDFFormat.RDFXML.getMIMEType());
			} else if (accept.contains(RDFFormat.TURTLE.getMIMEType())){
				location = localBaseUri+resID+".ttl";
				response.addHeader("Content-Type", RDFFormat.TURTLE.getMIMEType());
			} else if (accept.contains(RDFFormat.N3.getMIMEType())){
				location = localBaseUri+resID+".n3";
				response.addHeader("Content-Type", RDFFormat.N3.getMIMEType());
			} else if (accept.contains(RDFFormat.NTRIPLES.getMIMEType())){
				location = localBaseUri+resID+".nt";
				response.addHeader("Content-Type", RDFFormat.NTRIPLES.getMIMEType());
			} else {//html
				location = localBaseUri+resID+".html";
				response.addHeader("Content-Type", "text/html");
			}
		} else {
			location = localBaseUri+resID+".html";
			response.addHeader("Content-Type", "text/html");
		}
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", location);	
		context.responseComplete();
		return;
	}
	
	/**
	 * Based on the pageMapping defined by the user, it redirects to the page that describe the specific resource.
	 * To define a page mapping for a generic resource type or for other type not yet specified by the other
	 * page mapping, use a map-entry key:default, value:(name of the page to redirect).
	 * @throws Exception 
	 */
	public void redirect() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String resID = request.getParameter("resID");
		logger.debug("redirecting for resource with id "+resID);
		//mapping <tipoRisorsa-pagina>
		Set<String> types = pageMapping.keySet();
		for (String type : types){
			if (resType == null){
				resourceTypeDetect();
			}
			if (resType.equals(type)){
				/* Operazione necessaria: il presente bean ha scope di sessione. Se resType non venisse
				 * riportato a null, qualora dopo la richiesta della pagina html di una risorsa di tipo A,
				 * venisse richiesta la pagina di una risorsa di tipo B, resType sarebbe ancora A, la detect
				 * non sarebbe eseguita, e si verrebbe reindirizzati verso la pagina per descrivere risorse
				 * di tipo A, anzich� B. */
				resType = null;
				String page = pageMapping.get(type);
				context.getApplication().getNavigationHandler().handleNavigation(context, null, "/"+page+"?resID="+resID);
				return;
			}
			if (resType.equals("unknown")){//resource doesn't exist
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				context.getApplication().getNavigationHandler().handleNavigation(context, null, "/not_found.xhtml");
				return;
			}
		}
		if (pageMapping.containsKey("default")){
			String page = pageMapping.get("default");
			context.getApplication().getNavigationHandler().handleNavigation(context, null, "/"+page+"?resID="+resID);
			return;
		}
		throw new LoddyException("No page mapping specified for resource type " + resType + 
				". Please, check pageMapping property in WEB-INF/faces-config.xml");
	}
	
	/**
	 * Returns the link to the resource's description page in different format.
	 * Supported format: rdf, ttl, nt, n3.
	 * @param format
	 * @return
	 * @throws LoddyException
	 */
	public String getLinkForFormat(String format) throws LoddyException{
		if ((format.equals("rdf")) || (format.equals("ttl")) || (format.equals("nt")) || (format.equals("n3"))){
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			String resID = request.getParameter("resID");
			String link = connectionBean.getLocalBaseURI()+resID+"."+format;
			return  link;
		} else {
			throw new LoddyException("Invalid RDF format "+format+". Available format: rdf, ttl, nt, n3.");
		}
	}
	
	private void resourceTypeDetect() throws Exception {
		resType = "unknown";
		TripleQueryModelHTTPConnection conn = connectionBean.getConnection();
		
		String requestedResource = connectionBean.getRequestedResourceUri();
		String query = "select ?type where {<"+requestedResource+"> a ?type}";
		logger.debug("detecting resource type for "+requestedResource+"\n"+query);
		TupleQuery tq = conn.createTupleQuery(QueryLanguage.SPARQL, query, "");
		TupleBindingsIterator result = tq.evaluate(false);
		connectionBean.releaseConnection(conn);
		if (result.hasNext()){
			resType = result.next().getBinding("type").getBoundValue().getNominalValue();
			logger.debug("Resource type detected: " + resType);
		} else {
			logger.debug("No resource type found. Resousce " + requestedResource + " not exists");
		}
	}

}
