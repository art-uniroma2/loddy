package it.uniroma2.art.loddy.beans;

import java.io.OutputStream;
import java.util.Collection;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.GraphSource;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.utilities.RDFIterators;

public class RdfWriterBean {
	
	protected static Logger logger = LoggerFactory.getLogger(RdfWriterBean.class);
	
	private GraphSource graphSource;
	private ConnectionBean connectionBean;
	
	/**
	 * connectionBean property injection
	 * @param connectionBean
	 */
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}

	/**
	 * graphQuery property injection 
	 * @param graphQuery
	 */
	public void setGraphSource(GraphSource graphSource) {
		this.graphSource = graphSource;
	}
	
	/**
	 * Produces a response containing a serialized file of the graphSource. The serialization format is
	 * specified by a parameter contained in the request URL.
	 * @throws LoddyException
	 */
	public void printRdf() throws LoddyException{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String format = request.getParameter("format");
		if (format == null) //nel caso la pagina sia richiesta direttamente da URL (senza parametro ?format)
			format = "rdf";
		logger.debug("Returning description in "+format+" format");
		
		try {
			Collection<ARTStatement> stats = graphSource.getGraph();
			if (stats.isEmpty() && !resourceExists()){ //if the describe returned no statements, check if the resource exists
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				context.getApplication().getNavigationHandler().handleNavigation(context, null, "/not_found.xhtml");
				return;
			}
		
			OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(new ARTModelFactorySesame2Impl());		
			BaseRDFTripleModel model = fact.loadRDFBaseModel("", ".", fact.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class));
		    ExternalContext ec = context.getExternalContext();
		    OutputStream outputStream = ec.getResponseOutputStream();
		    HttpServletResponse response = (HttpServletResponse) ec.getResponse();
		    if (format.equals("rdf")){
		    	response.addHeader("Content-Type", RDFFormat.RDFXML.getMIMEType());
				model.writeRDF(RDFIterators.createARTStatementIterator(stats.iterator()), RDFFormat.RDFXML, outputStream);
		    } else if (format.equals("ttl")){
		    	response.addHeader("Content-Type", RDFFormat.TURTLE.getMIMEType());
				model.writeRDF(RDFIterators.createARTStatementIterator(stats.iterator()), RDFFormat.TURTLE, outputStream);
		    } else if (format.equals("n3")){
		    	response.addHeader("Content-Type", RDFFormat.N3.getMIMEType());
				model.writeRDF(RDFIterators.createARTStatementIterator(stats.iterator()), RDFFormat.N3, outputStream);
		    } else if (format.equals("nt")){
		    	response.addHeader("Content-Type", RDFFormat.NTRIPLES.getMIMEType());
				model.writeRDF(RDFIterators.createARTStatementIterator(stats.iterator()), RDFFormat.NTRIPLES, outputStream);
		    }
			context.responseComplete();
			model.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new LoddyException();
		}
	}
	
	private boolean resourceExists() throws Exception {
		TripleQueryModelHTTPConnection conn = connectionBean.getConnection();
		
		String requestedResource = connectionBean.getRequestedResourceUri();
		String query = "select ?type where {<"+requestedResource+"> a ?type}";
		logger.debug("detecting resource type for "+requestedResource+"\n"+query);
		TupleQuery tq = conn.createTupleQuery(QueryLanguage.SPARQL, query, "");
		TupleBindingsIterator result = tq.evaluate(false);
		connectionBean.releaseConnection(conn);
		if (result.hasNext()){
			return true;
		} else {
			return false;
		}
	}
	
}
