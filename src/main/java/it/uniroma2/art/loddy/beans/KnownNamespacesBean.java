package it.uniroma2.art.loddy.beans;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class KnownNamespacesBean {
	
private Map<String, String> map;
	
	private final String SKOSNamespace = "http://www.w3.org/2004/02/skos/core#";
	private final String SKOSPrefix = "skos";
	
	private final String SKOSXLNamespace = "http://www.w3.org/2008/05/skos-xl#";
	private final String SKOSXLPrefix = "skosxl";
	
	private final String OWLNamespace = "http://www.w3.org/2002/07/owl#";
	private final String OWLPrefix = "owl";
	
	private final String RDFSNamespace = "http://www.w3.org/2000/01/rdf-schema#";
	private final String RDFSPrefix = "rdfs";
	
	private final String RDFNamespace = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	private final String RDFPrefix = "rdf";
	
	private final String FOAFNamespace = "http://xmlns.com/foaf/0.1/";
	private final String FOAFPrefix = "foaf"; 
	
//	private final String Namespace = "";
//	private final String Prefix = "";
	
	public KnownNamespacesBean(){
		map = new HashMap<String, String>();
		map.put(SKOSNamespace, SKOSPrefix);
		map.put(SKOSXLNamespace, SKOSXLPrefix);
		map.put(OWLNamespace, OWLPrefix);
		map.put(RDFSNamespace, RDFSPrefix);
		map.put(RDFNamespace, RDFPrefix);
		map.put(FOAFNamespace, FOAFPrefix);
//		map.put(, ); 
	}
	
	/**
	 * namespacePrefixMapping property injection
	 * @param mapping
	 */
	public void setNamespacePrefixMapping(Map<String, String> mapping){
		map.putAll(mapping);
	}
	
	/**
	 * Returns a collection of the prefixes contained in the namespace-prefix map
	 * @return
	 */
	public Collection<String> getPrefixes(){
		return map.values();
	}
	
	/**
	 * Returns a collection of the namespaces contained in the namespace-prefix map
	 * @return
	 */
	public Set<String> getNamespaces(){
		return map.keySet();
	}
	
	/**
	 * Returns the namespace to which the specified prefix is mapped, or null if the map contains no mapping
	 * for the prefix
	 * @param prefix
	 * @return
	 */
	public String getNamespace(String prefix){
		String namespace = null;
		for (String pref : map.keySet()){
			if (map.get(pref).equals(prefix))
				prefix = pref;
		}
		return namespace;
	}
	
	/**
	 * Returns the prefix to which the specified namespace is mapped, or null if the map contains no mapping
	 * for the namespace
	 * @param namespace
	 * @return
	 */
	public String getPrefix(String namespace){
		return map.get(namespace);
	}
	
	/**
	 * Checks if the map contains the specified namespace
	 * @param namespace
	 * @return
	 */
	public boolean containsNamespace(String namespace){
		return map.containsKey(namespace);
	}
	
	/**
	 * Checks if the map contains the specified prefix
	 * @param prefix
	 * @return
	 */
	public boolean containsPrefix(String prefix){
		return map.containsValue(prefix);
	}

}
