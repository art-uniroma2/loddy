package it.uniroma2.art.loddy.beans;

import java.net.URL;
import java.util.Collection;

public class FormatterBean {
	
	private ConnectionBean connectionBean;
	private KnownNamespacesBean knownNamespaces;
	
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}
	
	public void setKnownNamespaces(KnownNamespacesBean knownNamespaces){
		this.knownNamespaces = knownNamespaces;
	}
	
	/**
	 * Format the resource URI if it has to be written as text on xhtml page.
	 * If resource is a property it tries to rewrite the URI in a prefixed form.
	 * @param resource
	 * @return
	 */
	public String formatText(String resource){
		String prefixed = resource;
		Collection<String> namespaces = knownNamespaces.getNamespaces();
		for (String ns : namespaces){
			if (resource.contains(ns)){
				String pref = knownNamespaces.getPrefix(ns);
				prefixed = resource.replace(ns, pref+":");
			}
		}
		return prefixed;
	}
	
	/**
	 * Format the resource URI if it has to be written as a link on xhtml page.
	 * If resource is an internal URIresource it returns it rewritten with a localBaseURI instead baseURI.
	 * Otherwise, if resource is not an internal URI, returns the same.  
	 * @param resource
	 * @return
	 */
	public String formatLink(String resource){
		if (resource.startsWith(connectionBean.getBaseURI())){//is internal resource
			return rewriteURL(resource);
		} else {
			return resource;
		}
	}
	
	/**
	 * Checks if the resource is an URL.
	 * Useful to check from xhtml page if an element of a statement must be rendered as a link or as plain text.
	 * @param resource
	 * @return
	 */
	public boolean isLink(String resource){
		//return resource.startsWith("http://");
		try {
			URL url = new URL(resource);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	//Restituisce l'URI della risorsa (se interna) con il localBaseURI al posto del baseURI
	private String rewriteURL(String res){
		String url;
		String baseURI = connectionBean.getBaseURI();
		String localBaseURI = connectionBean.getLocalBaseURI();
		if (res.startsWith(baseURI))
			url = res.replace(baseURI, localBaseURI);
		else
			url = res;
		return url;
	}

}
