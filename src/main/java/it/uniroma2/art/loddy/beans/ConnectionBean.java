package it.uniroma2.art.loddy.beans;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;

public class ConnectionBean {
	
	protected static Logger logger = LoggerFactory.getLogger(ConnectionBean.class);
	
	Queue<TripleQueryModelHTTPConnection> connectionPool;

	//managed property
	private String sparqlEndpointURL;
	private String baseURI;
	private String localBaseURI;
	
	private volatile int createdConnection = 0;
	private volatile int availableConnections = 0;
	private volatile int usedConnection = 0;
	
	public ConnectionBean(){
		connectionPool = new ConcurrentLinkedQueue<TripleQueryModelHTTPConnection>();
	}
	
	/*
	 * PostConstructor annotations is used to perform code once the dependency injection are done.
	 * The dependencies are injected after the constuctor and since the operations performed in this
	 * method use injected parameters, they could not be executed in the constructor.
	 */
	@PostConstruct
	public void startPoolHandler(){
		Runnable connStatusRunnable = new Runnable() {
		    public void run() {
		    	availableConnections = connectionPool.size();
		    	usedConnection = createdConnection - availableConnections;
				if (usedConnection < (float) createdConnection / 2){
					try {
						TripleQueryModelHTTPConnection conn = connectionPool.poll();
						conn.disconnect();
						createdConnection--;
					} catch (ModelAccessException e) {
						e.printStackTrace();
					}
				}
		    }
		};
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(connStatusRunnable, 0, 5, TimeUnit.SECONDS);
	}

	
	/**
	 * sparqlEndpointURL property injection
	 * @param sparqlEndpointURL
	 */
	public void setSparqlEndpointURL(String sparqlEndpointURL){
		this.sparqlEndpointURL = sparqlEndpointURL;
	}
	
	public String getSparqlEndpointURL(){
		return sparqlEndpointURL;
	}
	
	/**
	 * baseURI property injection
	 * @param baseURI
	 */
	public void setBaseURI(String baseURI){
		this.baseURI = baseURI;
	}
	
	public String getBaseURI(){
		return baseURI;
	}
	
	/**
	 * localBaseURI property injection
	 * @param localBaseURI
	 */
	public void setLocalBaseURI(String localBaseURI){
		this.localBaseURI = localBaseURI;
	}
	
	public String getLocalBaseURI(){
		return localBaseURI;
	}
	
	/**
	 * Returns the resourceURI involved in the request
	 * @return
	 */
	public String getRequestedResourceUri(){
		HttpServletRequest req = ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest());
		String resID = req.getParameter("resID");
		if (resID == null){
			
		}
		return (baseURI + resID);
	}

	public TripleQueryModelHTTPConnection getConnection() throws ModelCreationException{
		TripleQueryModelHTTPConnection conn = connectionPool.poll();
		if (conn == null) //no connection available at the moment => create a new one
			conn = createConnection();
		return conn;
	}
	
	public synchronized void releaseConnection(TripleQueryModelHTTPConnection conn){
		connectionPool.add(conn);
	}
	
	private synchronized TripleQueryModelHTTPConnection createConnection() throws ModelCreationException{
		TripleQueryModelHTTPConnection conn;
		OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(new ARTModelFactorySesame2Impl());
		conn = fact.loadTripleQueryHTTPConnection(sparqlEndpointURL);
		logger.debug("connected to "+sparqlEndpointURL);
		createdConnection++;
		return conn;
	}
	
	

}
