package it.uniroma2.art.loddy.beans;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDFS;
import it.uniroma2.art.owlart.vocabulary.SKOS;
import it.uniroma2.art.owlart.vocabulary.SKOSXL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BestChoiceLabelBean {
	
	protected static Logger logger = LoggerFactory.getLogger(BestChoiceLabelBean.class);
	
	//managed properties
	private List<String> models; //priority list of desired label models (admitted value: SKOSXL, SKOS, RDFS)
	private List<String> langs; //priority list of desired languages 
	private ConnectionBean connectionBean;
	
	private static final Map<String, String> modelConversionMap;
    static {
    	modelConversionMap = new HashMap<String, String>();
        modelConversionMap.put("SKOSXL", SKOSXL.NAMESPACE);
        modelConversionMap.put("SKOS-XL", SKOSXL.NAMESPACE);
        modelConversionMap.put("SKOS", SKOS.NAMESPACE);
        modelConversionMap.put("RDFS", RDFS.NAMESPACE);
    }
	
	/**
	 * connectionBean property injection.
	 * @param connectionBean
	 */
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}
	
	/**
	 * models property injection
	 * @param models
	 */
	public void setModels(List<String> models){
		this.models = models;
	}
	
	/**
	 * langs property injection
	 * @param langs
	 */
	public void setLangs(List<String> langs){
		this.langs = langs;
	}
	
	public String getBestChoiceLabel() throws LoddyException {
		Collection<ARTStatement> graph = doQuery();
		graph = filterGraphForLanguage(graph);
		graph = filterGraphForModel(graph);
		Iterator<ARTStatement> it = graph.iterator();
		if (it.hasNext())
			return it.next().getObject().asLiteral().getLabel();
		else
			return "";
	}
	
	/**
	 * Constructs a graph composed by requested resource (subject), label predicate and label (object). 
	 * @return
	 * @throws LoddyException
	 */
	private Collection<ARTStatement> doQuery() throws LoddyException{
		String requestedResource = connectionBean.getRequestedResourceUri();
		String query = "CONSTRUCT {\n"
			+ "<" + requestedResource + "> ?pred ?label .\n"
			+ "} WHERE { { <" + requestedResource + "> ?pred _:v .\n"
			+ "_:v <" + SKOSXL.LITERALFORM + "> ?label .\n"
			+ "FILTER (?pred = <" + SKOSXL.PREFLABEL + ">)\n"
			+ "} UNION {\n"
			+ "<" + requestedResource + "> ?pred ?label .\n"
			+ "FILTER (?pred = <" + SKOS.PREFLABEL + "> ||\n"
			+ "?pred = <" + RDFS.LABEL + "> ) } }";
		try {
			TripleQueryModelHTTPConnection conn = connectionBean.getConnection();
			it.uniroma2.art.owlart.query.GraphQuery gq = conn.createGraphQuery(
					QueryLanguage.SPARQL, query, "");
			logger.debug("evaluating graph query\n"+query);
			ARTStatementIterator itRes = gq.evaluate(false);
			connectionBean.releaseConnection(conn);
			return RDFIterators.getCollectionFromIterator(itRes);
		} catch (Exception e) {
			e.printStackTrace();
			throw new LoddyException();
		}
	}
	
	private Collection<ARTStatement> filterGraphForLanguage(Collection<ARTStatement> graph){
		Collection<ARTStatement> outGraph = new ArrayList<ARTStatement>();
		boolean langFound = false;
		if (langs != null && langs.size()>0){
			for (String l : langs){ //for every lang
				for (ARTStatement stat : graph){ //for every statement
					if (stat.getObject().asLiteral().getLanguage().equals(l)){ //if the language is the desired one
						outGraph.add(stat); //add the statement to the output graph
						langFound = true; //and make the loop stop, so it doesn't search for other languages
					}
				}
				if (langFound) break;
			}
		} else { //if langs list is not provided or is empty, get language from context
			String l = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale().getLanguage();
			for (ARTStatement stat : graph){ //for every statement
				if (stat.getObject().asLiteral().getLanguage().equals(l)){ //if the language is the desired one
					outGraph.add(stat); //add the statement to the output graph
					langFound = true; //and make the loop stop
				}
			}
		}
		return outGraph;
	}
	
	private Collection<ARTStatement> filterGraphForModel(Collection<ARTStatement> graph){
		Collection<ARTStatement> outGraph = new ArrayList<ARTStatement>();
		boolean modelFound = false;
		if (models == null)
			models = new ArrayList<String>(); //just to avoid NullPointerException if the user doesn't define the list
		for (String m : models){
			for (ARTStatement stat : graph){
				if (stat.getPredicate().getNominalValue().startsWith(modelConversionMap.get(m.toUpperCase()))){
					outGraph.add(stat);
					modelFound = true;
				}
			}
			if (modelFound)
				break;
		}
		return outGraph;
	}
	
	

}
