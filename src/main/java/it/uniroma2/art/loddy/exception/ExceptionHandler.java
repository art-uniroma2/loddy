package it.uniroma2.art.loddy.exception;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class ExceptionHandler {
	
	public String getStatusCode(){
		return String.valueOf((Integer)FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.status_code"));
	}

	public String getMessage(){
		return FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.message").toString();
	}

	public String getExceptionType(){
		return ((Exception)FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.exception")).getClass().getName();
	}

	public String getException(){
		return ((Exception)FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.exception")).toString();
	}

	public String getRequestURI(){
		return FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.request_uri").toString();
	}

	public String getServletName(){
		return FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get("javax.servlet.error.servlet_name").toString();
	}

}
