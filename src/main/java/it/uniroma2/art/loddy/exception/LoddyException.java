package it.uniroma2.art.loddy.exception;

public class LoddyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6108326889579602198L;

	public LoddyException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoddyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public LoddyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LoddyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LoddyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
