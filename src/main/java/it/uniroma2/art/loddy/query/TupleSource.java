package it.uniroma2.art.loddy.query;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.owlart.query.TupleBindings;

import java.util.Collection;

/**
 * Bean that provides a collection of TupleBindings.
 * @author Tiziano
 *
 */
public interface TupleSource {
	
	/**
	 * Returns a collection of TupleBindings
	 * @return
	 * @throws LoddyException
	 */
	public Collection<TupleBindings>getTuples() throws LoddyException;

}
