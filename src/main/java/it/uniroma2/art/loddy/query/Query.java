package it.uniroma2.art.loddy.query;

/**
 * Bean that can evaluate a generic sparql query
 * @author Tiziano Lorenzetti
 *
 */
public interface Query {
	
	/**
	 * query property injection. Sets the query to be evaluated.
	 * @param query
	 */
	public void setQuery(String query);

}
