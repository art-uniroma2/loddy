package it.uniroma2.art.loddy.query;


/**
 * 
 * @author Tiziano Lorenzetti
 *
 */
public interface TupleQuery extends TupleSource, Query{

}
