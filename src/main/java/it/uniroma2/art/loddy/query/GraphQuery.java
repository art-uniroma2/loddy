package it.uniroma2.art.loddy.query;

/**
 * 
 * @author Tiziano Lorenzetti
 *
 */
public interface GraphQuery extends GraphSource, Query{

}
