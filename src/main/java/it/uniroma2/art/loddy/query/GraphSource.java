package it.uniroma2.art.loddy.query;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.owlart.model.ARTStatement;

import java.util.Collection;

/**
 * Bean that provides a collection of ARTStatement.
 * @author Tiziano
 *
 */
public interface GraphSource {
	
	/**
	 * Returns a collection of ARTStatement
	 * @return
	 * @throws LoddyException
	 */
	public Collection<ARTStatement> getGraph() throws LoddyException;

}
