package it.uniroma2.art.loddy.query.impl;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.loddy.beans.ConnectionBean;
import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.TupleQuery;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.utilities.RDFIterators;

public class TupleQueryBean implements TupleQuery{
	
	protected static Logger logger = LoggerFactory.getLogger(TupleQueryBean.class);
	
	private ConnectionBean connectionBean;
	private String query;
	private Collection<TupleBindings> tuples;
	
	/**
	 * connectionBean property injection
	 * @param connectionBean
	 */
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}
	
	@Override
	public void setQuery(String query){
		this.query = query;
	}
	
	@Override
	public Collection<TupleBindings> getTuples () throws LoddyException{
		if (tuples == null)
			doQuery();
		return tuples;	
	}
	
	private void doQuery() throws LoddyException{
		if (query == null)
			throw new LoddyException("No query setted");
		try {
			TripleQueryModelHTTPConnection conn = connectionBean.getConnection();
			it.uniroma2.art.owlart.query.TupleQuery tq = conn.createTupleQuery(
					QueryLanguage.SPARQL, query, "");
			logger.debug("evaluating tuple query\n"+query);
			TupleBindingsIterator itRes = tq.evaluate(false);
			connectionBean.releaseConnection(conn);
			tuples = RDFIterators.getCollectionFromIterator(itRes);
		} catch (Exception e) {
			logger.debug(e.getClass().getName());
			throw new LoddyException(e);
		}
	}

}
