package it.uniroma2.art.loddy.query.impl;

import it.uniroma2.art.loddy.beans.ConnectionBean;
import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.GraphQuery;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.utilities.RDFIterators;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GraphQueryBean implements GraphQuery{
	
	protected static Logger logger = LoggerFactory.getLogger(GraphQueryBean.class);
	
	private ConnectionBean connectionBean;
	private String query;
	private Collection<ARTStatement> graph;

	/**
	 * connectionBean property injection.
	 * @param connectionBean
	 */
	public void setConnectionBean(ConnectionBean connectionBean){
		this.connectionBean = connectionBean;
	}
	
	@Override
	public void setQuery(String query){
		this.query = query;
	}
	
	@Override
	public Collection<ARTStatement> getGraph() throws LoddyException{
		if (graph == null)
			doQuery();
		return graph;
	}
	
	private void doQuery() throws LoddyException{
		if (query == null){
			String requestedResourceUri = connectionBean.getRequestedResourceUri();
			query = "describe <"+requestedResourceUri+">";
		}
		try {
			TripleQueryModelHTTPConnection conn = connectionBean.getConnection();
			it.uniroma2.art.owlart.query.GraphQuery gq = conn.createGraphQuery(
					QueryLanguage.SPARQL, query, "");
			logger.debug("evaluating graph query\n"+query);
			ARTStatementIterator itRes = gq.evaluate(false);
			connectionBean.releaseConnection(conn);
			graph = RDFIterators.getCollectionFromIterator(itRes);
		} catch (Exception e) {
			logger.debug(e.getClass().getName());
			throw new LoddyException(e);
		}
	}

}
