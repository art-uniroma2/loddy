package it.uniroma2.art.loddy.predicate;

import it.uniroma2.art.owlart.query.TupleBindings;

public interface TuplePredicate {
	
	/**
	 * Checks if the predicate is satisfied by the input tuple
	 * @param tuples
	 * @return
	 */
	public boolean apply(TupleBindings tuple);

}
