package it.uniroma2.art.loddy.predicate.impl;

import it.uniroma2.art.loddy.predicate.StatementPredicate;
import it.uniroma2.art.owlart.model.ARTStatement;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * Implementation of StatementPredicate. It allows to specified a list of values for every element of a
 * statement (subject, predicate, object).
 * @author Tiziano Lorenzetti
 *
 */
public class ThreeBagsAdmittedValuesStatementPredicate implements StatementPredicate {
	
	private ListMultimap<String, String> valuesMap;
	private static final String SUBJECT_KEY = "1";
	private static final String PREDICATE_KEY = "2";
	private static final String OBJECT_KEY = "3";
	
	public ThreeBagsAdmittedValuesStatementPredicate() {
		valuesMap = ArrayListMultimap.create();
	}
	
	/**
	 * Prperty injection. Sets a list of values for the subject
	 * @param values
	 */
	public void setSubjectValues(List<String> values){
		for (String v : values)
			valuesMap.put(SUBJECT_KEY, v);
	}
	
	/**
	 * Prperty injection. Sets a list of values for the predicate
	 * @param values
	 */
	public void setPredicateValues(List<String> values){
		for (String v : values)
			valuesMap.put(PREDICATE_KEY, v);
	}
	
	/**
	 * Prperty injection. Sets a list of values for the object
	 * @param values
	 */
	public void setObjectValues(List<String> values){
		for (String v : values)
			valuesMap.put(OBJECT_KEY, v);
	}
	
	/**
	 * Returns true if the values are satisfied by the input statement. Values on the same element of the
	 * statement are evaluated in OR, values on all the elements are evaluated in AND. Thus, the whole
	 * predicate is satisfied if at least one value for every single element of the statement is satisfied.
	 * If an element of the statement has no value specified, it will not be evaluated.
	 * 
	 * @param stat
	 * @return
	 */
	public boolean apply(ARTStatement stat){
		Collection<String> subjectValues = valuesMap.get(SUBJECT_KEY);
		Collection<String> predicateValues = valuesMap.get(PREDICATE_KEY);
		Collection<String> objectValues = valuesMap.get(OBJECT_KEY);
		if (!subjectValues.isEmpty()){//se ho filtri sul soggetto
			boolean predicateSat = false;
			for (String v : subjectValues){//li scorro
				if (v.equals(stat.getSubject().getNominalValue())){//se il filtro sul soggetto � soddisfatto dallo statement
					predicateSat = true; //segnalo questa cosa
					break;
				}
			}
			if (!predicateSat)
				return false;
		}
		if (!predicateValues.isEmpty()){
			boolean predicateSat = false;
			for (String v : predicateValues){
				if (v.equals(stat.getPredicate().getNominalValue())){
					predicateSat = true;
					break;
				}
			}
			if (!predicateSat)
				return false;
		}
		if  (!objectValues.isEmpty()){
			boolean predicateSat = false;
			for (String v : objectValues){
				if (v.equals(stat.getObject().getNominalValue())){
					predicateSat = true;
					break;
				}
			}
			if (!predicateSat)
				return false;
		}
		return true;
	}
	

}
