package it.uniroma2.art.loddy.predicate.impl;

import it.uniroma2.art.loddy.predicate.TuplePredicate;
import it.uniroma2.art.owlart.query.TupleBindings;

import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multiset;

public class SimpleTuplePredicate implements TuplePredicate{
	
	private ListMultimap<String, String> filterMap;
	
	public SimpleTuplePredicate(){
		filterMap = ArrayListMultimap.create();
	}
	
//	public void setFilterMap(Map<String, String> filterMap){
	public void setFilterMap(ListMultimap<String, String> filterMap){
		this.filterMap = filterMap;
//		Set<String> keys = filterMap.keySet();
//		for (String k : keys){
//			String value = filterMap.get(k);
//			this.filterMap.put(k, value);
//		}
	}
	
	/**
	 * Returns true if all the filters are satisfied. 
	 * @param stat
	 * @return
	 */
	public boolean apply(TupleBindings tupleBinding){
		Multiset<String> bindingNames = filterMap.keys();
		for (String bindingName : bindingNames){
			boolean filterSat = false;
			List<String> filters = filterMap.get(bindingName);
			String bindingValue = tupleBinding.getBinding(bindingName).getBoundValue().getNominalValue(); 
			if (bindingValue != null){
				for (String f : filters){
					if (bindingValue.equals(f)){
						filterSat = true;
						break;
					}
				}
			}
			if (!filterSat) 
				return false;
		}
		return true;
	}

}
