package it.uniroma2.art.loddy.predicate;

import it.uniroma2.art.owlart.model.ARTStatement;

public interface StatementPredicate {
	
	/**
	 * Checks if the predicate is satisfied by the input statement
	 * @param statement
	 * @return
	 */
	public boolean apply(ARTStatement statement);

}
