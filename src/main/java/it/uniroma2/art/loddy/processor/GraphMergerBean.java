package it.uniroma2.art.loddy.processor;

import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.GraphSource;
import it.uniroma2.art.owlart.model.ARTStatement;

public class GraphMergerBean implements GraphSource{
	
	private GraphSource graphSource;
	private GraphSource graphSourceToIntegrate;
	private Collection<ARTStatement> mergedGraph;

	/**
	 * graphSource property injection. It sets the first graph source to integrate.
	 * @param graphSource
	 */
	public void setGraphSource(GraphSource graphSource) {
		this.graphSource = graphSource;
	}
	
	/**
	 * graphSourceToIntegrate property injection. It sets the second graph source to integrate.
	 * @param graphSource
	 */
	public void setGraphSourceToIntegrate(GraphSource graphSource){
		graphSourceToIntegrate = graphSource;
	}

	@Override
	public Collection<ARTStatement> getGraph() throws LoddyException {
		if (mergedGraph == null){
			mergedGraph = new ArrayList<ARTStatement>();
			mergedGraph.addAll(graphSource.getGraph());
			mergedGraph.addAll(graphSourceToIntegrate.getGraph());
		}
		return mergedGraph;
	}

}
