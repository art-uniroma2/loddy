package it.uniroma2.art.loddy.processor;

import java.util.ArrayList;
import java.util.Collection;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.GraphSource;
import it.uniroma2.art.loddy.structure.GroupedStatement;
import it.uniroma2.art.owlart.model.ARTStatement;

public class GraphGrouperBean {
	
	private GraphSource graphSource;
	
	/**
	 * graphSource property injection. It sets the graph source to be grouped.
	 * @param graphSource
	 */
	public void setGraphSource(GraphSource graphSource){
		this.graphSource = graphSource;
	}
	
	/**
	 * Returns a collection on GroupedStatement that represents the input collection of statement, but grouped
	 * by subject, then by predicate. See GroupedStatement.
	 * @return
	 * @throws LoddyException
	 */
	public Collection<GroupedStatement> group() throws LoddyException{
		Collection<ARTStatement> statements = graphSource.getGraph();
		Collection<GroupedStatement> coll = new ArrayList<GroupedStatement>();
		for (ARTStatement stat : statements){
			boolean exists = false;
			String subj = stat.getSubject().getNominalValue();
			String pred = stat.getPredicate().getNominalValue();
			String obj = stat.getObject().getNominalValue();
			for (GroupedStatement gs : coll){
				if (gs.getSubject().equals(subj)){
					gs.addPredObj(pred, obj);
					exists = true;
					break;
				}
			}
			if (!exists){
				GroupedStatement gs = new GroupedStatement();
				gs.setSubject(subj);
				gs.addPredObj(pred, obj);
				coll.add(gs);
			}
		}
		return coll;
	}
}
