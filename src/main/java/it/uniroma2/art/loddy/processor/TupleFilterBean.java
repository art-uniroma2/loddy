package it.uniroma2.art.loddy.processor;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.predicate.TuplePredicate;
import it.uniroma2.art.loddy.query.TupleSource;
import it.uniroma2.art.owlart.query.TupleBindings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TupleFilterBean implements TupleSource{
	
	private TupleSource tupleSource;
	private Collection<TuplePredicate> predicates;
	private Collection<TupleBindings> processed;
	private boolean passOnly = false;

	/**
	 * tupleSource property injection. It provides the tupleSource to be filtered
	 * @param tupleSource
	 */
	public void setTupleSource(TupleSource tupleSource) {
		this.tupleSource = tupleSource;
	}

	/**
	 * predicates property injection. It provides the predicates to checks by the filter.
	 * @param predicates
	 */
	public void setPredicates(List<TuplePredicate> predicates) {
		this.predicates = predicates;
	}
	
	/**
	 * passOnly property injection. If true, this bean filters let pass only the tuples that satisfies the
	 * predicates. Otherwise, if false, it works on "delete only" mode filtering out that tuples. 
	 * @param passOnly
	 */
	public void setPassOnly(boolean passOnly) {
		this.passOnly = passOnly;
	}
	
	@Override
	public Collection<TupleBindings> getTuples() throws LoddyException {
		if (processed == null){
			processed = new ArrayList<TupleBindings>();
			Collection<TupleBindings> tupleBindings = tupleSource.getTuples();
			for (TupleBindings tuple : tupleBindings){
				boolean predSat = false;
				for (TuplePredicate pred : predicates){
					if (pred.apply(tuple)){
						predSat = true;
						break;
					}
				}
				if (predSat == passOnly)
					processed.add(tuple);
			}	
		}
		return processed;
	}




}
