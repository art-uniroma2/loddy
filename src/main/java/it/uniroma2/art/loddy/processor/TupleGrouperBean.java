package it.uniroma2.art.loddy.processor;

import java.util.Collection;
import java.util.List;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.TupleSource;
import it.uniroma2.art.loddy.structure.TupleTree;
import it.uniroma2.art.owlart.query.TupleBindings;

public class TupleGrouperBean {

	private TupleSource tupleSource;
	private List<String> bindings;

	/**
	 * tupleSource property injection. It sets the tuple source to be grouped.
	 * 
	 * @param tupleSource
	 */
	public void setTupleSource(TupleSource tupleSource) {
		this.tupleSource = tupleSource;
	}

	/**
	 * bindings property injection. It sets the list of bindings on which base the grouping. Note that these
	 * bindings must be a subset (or all) of the bindings used in the tupleSource's query and they all must be
	 * ground, otherwise a null pointer exception would be thrown.
	 * TODO: simply remove tuples where there's not a match with the bindings
	 * 
	 * @param bindings
	 */
	public void setBindings(List<String> bindings) {
		this.bindings = bindings;
	}

	/**
	 * Returns a collection on GroupedStatement that represents the input collection of statement, but
	 * grouped, in order, by the bindings passed as a list.
	 * 
	 * @return
	 * @throws LoddyException
	 */
	public TupleTree group() throws LoddyException {
		TupleTree tree = new TupleTree(bindings);
		Collection<TupleBindings> tuples = tupleSource.getTuples();
		for (TupleBindings t : tuples) {
			tree.addTuple(t);
		}
		return tree;
	}

}
