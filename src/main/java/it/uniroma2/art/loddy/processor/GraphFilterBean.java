package it.uniroma2.art.loddy.processor;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.predicate.StatementPredicate;
import it.uniroma2.art.loddy.query.GraphSource;
import it.uniroma2.art.owlart.model.ARTStatement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GraphFilterBean implements GraphSource{
	
	private Collection<StatementPredicate> predicates;
	private GraphSource graphSource;
	private Collection<ARTStatement> graph;
	private boolean passOnly = false;//true: filter applied in "pass only" mode
									//false: in "remove only" mode
	
	/**
	 * graphSource property injection. It provides the graphSource to be filtered
	 * @param graphSource
	 */
	public void setGraphSource(GraphSource graphSource) {
		this.graphSource = graphSource;
	}

	/**
	 * predicates property injection. It provides the predicates to checks by the filter.
	 * @param predicates
	 */
	public void setPredicates(List<StatementPredicate> predicates) {
		this.predicates = predicates;
	}

	/**
	 * passOnly property injection. If true, this bean filters let pass only the statements that satisfies the
	 * predicates. Otherwise, if false, it works on "delete only" mode filtering out that statements. 
	 * @param passOnly
	 */
	public void setPassOnly(boolean passOnly) {
		this.passOnly = passOnly;
	}

	@Override
	public Collection<ARTStatement> getGraph() throws LoddyException {
		if (graph == null){
			graph = new ArrayList<ARTStatement>();
			Collection<ARTStatement> statements = graphSource.getGraph();			
			for (ARTStatement stat : statements){
				boolean predSat = false;
				for (StatementPredicate pred: predicates){
					if (pred.apply(stat)){
						predSat = true;
						break;
					}
				}
				if (predSat == passOnly)
					graph.add(stat);
			}
			
		}
		return graph;
	}

}
