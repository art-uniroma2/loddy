package it.uniroma2.art.loddy.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import it.uniroma2.art.loddy.exception.LoddyException;
import it.uniroma2.art.loddy.query.GraphSource;
import it.uniroma2.art.owlart.model.ARTStatement;

/**
 * This bean sorts a GraphSource (collection of statements) based on a list of predicate. The order of the
 * predicates in the list determines the order of the statements into the collection. The statements
 * that have the specified predicates will be put on the top of the list.
 * @author Tiziano Lorenzetti
 *
 */
public class GraphSorterBean implements GraphSource {
	
	private List<String> sortingPredicates;
	private GraphSource graphSource;
	private Collection<ARTStatement> sortedGraph;
	
	/**
	 * graphSource property injection. It sets the first graph source to integrate.
	 * @param graphSource
	 */
	public void setGraphSource(GraphSource graphSource) {
		this.graphSource = graphSource;
	}
	
	/**
	 * sortingPredicates property injection. It sets the list of predicates on which base the sorting 
	 * @param predicates
	 */
	public void setSortingPredicates(List<String> predicates){
		this.sortingPredicates = predicates;
	}
	
	@Override
	public Collection<ARTStatement> getGraph() throws LoddyException {
		if (sortedGraph == null){
			sort(graphSource);
		}
		return sortedGraph;
	}
	
	private void sort(GraphSource graph) throws LoddyException{
		Collection<ARTStatement> graphStatements = graph.getGraph();
		sortedGraph = new ArrayList<ARTStatement>();
		for (String p : sortingPredicates){ //for every sorting predicate
			Iterator<ARTStatement> it = graphStatements.iterator();
			while (it.hasNext()){
				ARTStatement s = it.next();
				if (s.getPredicate().getNominalValue().equals(p)){ //if it's found
					sortedGraph.add(s);//add it to the sorted graph
					it.remove();//and remove it from the graphStatement
				}
			}
		}
		sortedGraph.addAll(graphStatements); //finally add all the remaining statements
	}

}
