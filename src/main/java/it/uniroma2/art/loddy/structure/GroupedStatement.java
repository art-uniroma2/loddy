package it.uniroma2.art.loddy.structure;

import java.util.Collection;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

/**
 * Class that represents Statements grouped by subject-predicate pair. Given a subject, it contains a
 * collection of predicates. Then given a predicate, it contains a collection of objects.
 * 
 * @author Tiziano
 * 
 */
public class GroupedStatement {

	private String subject;
	private Multimap<String, String> predObj;// keys are the predicates, values are the objects

	public GroupedStatement() {
		predObj = LinkedListMultimap.create();
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Returns the subject of the GroupedStatement
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	public void addPredObj(String predicate, String object) {
		predObj.put(predicate, object);
	}

	/**
	 * Returns a collection of predicates about the GroupedStatement
	 * @return
	 */
	public Collection<String> getPredicates() {
		return predObj.keySet();
	}

	/**
	 * Returns a collection of objects about the predicate specified.
	 * @param predicate
	 * @return
	 */
	public Collection<String> getObjects(String predicate) {
		return predObj.get(predicate);
	}
}
