package it.uniroma2.art.loddy.structure;

import it.uniroma2.art.owlart.query.TupleBindings;

import java.util.ArrayList;
import java.util.List;

public class Node {
	
	private String binding;
	private String value;
	private List<Node> children;
	private List<TupleBindings> tuples;
	
	public Node(){
		children = new ArrayList<Node>();
		tuples = new ArrayList<TupleBindings>();
	}
	
	public Node(String binding, String value){
		children = new ArrayList<Node>();
		tuples = new ArrayList<TupleBindings>();
		this.binding = binding;
		this.value = value;
	}
	
	public void setBinding(String binding){
		this.binding = binding;
	}
	
	public String getBinding(){
		return binding;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
	
	public void appendChild(Node child){
		children.add(child);
	}
	
	public List<Node> getChildren(){
		return children;
	}
	
	public Node getChild(Node child){
		for (Node c : children) {
			if (c.equals(child)){
				return c;
			}
		}
		return null;
	}
	
	//JSF Expression Language does not support overloading, so if this method is called getChild, and is invoked
	//from xhtml page, the above method will be executed (first method in the class that matches the name)
	public Node getChildWithValue(String value){
		for (Node c : children) {
			if (c.getValue().equals(value)){
				return c;
			}
		}
		return null;
	}
	
	public boolean hasChildren(){
		return (!children.isEmpty());
	}
	
	public void appendTuple(TupleBindings child){
		tuples.add(child);
	}
	
	public List<TupleBindings> getTuples(){
		return tuples;
	}
	
	public boolean hasTuples(){
		return (!tuples.isEmpty());
	}
	
	public boolean equals(Node node){
		return (node.getBinding().equals(binding) && node.getValue().equals(value));
	}
	
	public String toString(){
		return "[" + binding + ", " + value + "]";
	}

}
