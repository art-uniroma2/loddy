package it.uniroma2.art.loddy.structure;

import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.owlart.query.TupleBindings;

public class TupleTree {
	
	private Node root;
	private List<Node> nodes;
	
	private List<String> bindings;
	
	public TupleTree(List<String> bindings){
		this.bindings = bindings;
		root = new Node("root", "root");
		nodes = new ArrayList<Node>();
		nodes.add(root);
	}
	
	public Node getRootNode(){
		return root;
	}
	
	public void addTuple(TupleBindings tuple){
		Node parent, child;
		parent = root;
		for (String bind : bindings){
			String value = tuple.getBinding(bind).getBoundValue().getNominalValue();
			child = parent.getChild(new Node(bind, value));
			if (child == null) { //child node is not yet created
				child = new Node(bind, value);
				parent.appendChild(child);
			}
			parent = child;
		}
		parent.appendTuple(tuple);
	}

}
