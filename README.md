# Loddy #

Loddy is a lightweight system for supporting publication of Linked Open Data realized by the [ART Research Group](http://art.uniroma2.it) at the University of Rome Tor Vergata.

The project started in 2014 and, after a first release, its development has been slowed due to other projects with higher priority, still leaving final touches to be finalized. However, though with some limitations, LODDY can already be used for easily publishing RDF data through descriptive HTML pages. For this reason, we have packed up a demo, which is completely configured for working on a local machine, exposing data from the Agrovoc [1] SPARQL endpoint

## Building the project ##
### Requirements ###
1. a properly installed JDK version 8
2. a recent version of Apache Maven, preferably version 3.3.3 or superior (see section troubleshooting below)

### Building ###
just a plain:

```
#!cmd

mvn clean install
```

should suffice. 

### Troubleshooting ###

Pls refere to the PDF manual distributed with the system


[1] C. Caracciolo, A. Stellato, A. Morshed, G. Johannsen, S. Rajbhandari, Y. Jaques e J. Keizer, «The AGROVOC Linked Dataset,» Semantic Web Journal, vol. 4, n. 3, p. 341–348, 2013. 